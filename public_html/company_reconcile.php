<?php
/** jneubert, 2023-07-31
 *
 * three modes:
 * 1) empty page => offer search
 * 2) name, location and all other properties filled in => offer selection
 * 3) item selected => create QS
 */

require __DIR__ . '/vendor/autoload.php';

$title = "Match PM20 Companies with Wikidata";

output_header($title);

// global logic
// for GET requests (run mode 1), a page with only header and footer is
// created, no debug info
if ($_SERVER["REQUEST_METHOD"] == "POST") {

  // run mode 3
  if (isset($_POST["item"])) {
    runmode_3();
  } else {
    runmode_2();
  }
  //$debug=1;
}

output_footer($debug);

//////////////////

function runmode_2() {

  // Retrieve form data
  $company_name = htmlspecialchars($_POST["company_name"]);
  $search_name = htmlspecialchars($_POST["search_name"]);
  if ($search_name !== '') {
    $name = $search_name;
  } else {
    $name = $company_name;
  }

  $api = new RestClient();
  $service_url = 'https://wikidata.reconci.link/en/api';

  $request = array(
    'q0' => array(
      'query' => "$name",
      'type' => 'Q43229'
    )
  );

  $result = $api->get($service_url, [ "queries" => json_encode($request) ]) ;

  $matches = json_decode($result->{'response'})->{'q0'}->{'result'};

  output_fieldset($matches);
}

function runmode_3() {

  // Retrieve form data
  $company_name = htmlspecialchars($_POST["company_name"]);
  $location = htmlspecialchars($_POST["location"]);
  $signature = htmlspecialchars($_POST["signature"]);
  $no_of_images = htmlspecialchars($_POST["no_of_images"]);
  $date = htmlspecialchars($_POST["date"]);
  $folder_id = htmlspecialchars($_POST["folder_id"]);
  $search_name = htmlspecialchars($_POST["search_name"]);
  $item = htmlspecialchars($_POST["item"]);

  // statement for film section id property
  $section = "P11822|\"$location\"|P528|\"$signature\"";
  if ($no_of_images !== '') {
    $section = $section . '|P1104|' . $no_of_images;
  }
  if ($date !== '') {
    $section = $section . '|P580|' . format_wd_date($date);
  }
  if ($folder_id !== '') {
    $section = $section . '|P4293|' . "\"$folder_id\"";
  }
  $section = $section . '|P1810|' . "\"$company_name\"";

  if ($item == 'new') {
    $qs =<<<EOL5
      CREATE
      LAST|Len|"$company_name"
      LAST|Lde|"$company_name"
      LAST|Den|"company"
      LAST|Dde|"Unternehmen"
      LAST|P31|Q4830453|S248|Q36948990
      LAST|$section
      EOL5;
  } else {
    $qs = "$item|$section";
  }
  echo "<h3>Statement for copy&amp;paste to <a href='https://quickstatements.toolforge.org/#'>QuickStatements</a></h3>";
  echo "<p>For new items, description and type (P31) may be edited before insertion</p>";
  echo "<textarea id=\"qs\" name=\"qs\" rows=\"8\" cols=\"90\">$qs</textarea>";
}


function output_header($title) {

  // Retrieve form data
  $company_name = htmlspecialchars($_POST["company_name"]);
  $location = htmlspecialchars($_POST["location"]);
  $signature = htmlspecialchars($_POST["signature"]);
  $no_of_images = htmlspecialchars($_POST["no_of_images"]);
  $date = htmlspecialchars($_POST["date"]);
  $folder_id = htmlspecialchars($_POST["folder_id"]);
  $search_name = htmlspecialchars($_POST["search_name"]);

echo <<<EOL
<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="UTF-8">
  <title>$title</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes"/>
  <meta name="robots" content="noindex, nofollow"/>
  <link rel="stylesheet" href="/styles/simple.css" />
</head>
<body>

<h1>$title</h1>

<form action="company_reconcile.php" method="post">

<input id="location" name="location" type="text" value="$location" placeholder="location" required="required" pattern="h[12]/co/[AF]\d{4}H(_[12])?/\d{4}(/[LR])?" />
<label for="location"> Location on film (e.g., "h1/co/A0032H/0232" - copy from browser address bar)</label>

<input id="company_name" name="company_name" type="text" placeholder="name of entity to reconcile" value="$company_name" required="required" />
<label for="company_name"> Exact name of company or organization</label>

<input id="signature" name="signature" type="text" placeholder="signature" value="$signature" required="required" />
<label for="signature"> Signature of company or organization (e.g., "A10 12" - normally written on the upper right corner of the sheet)</label>

<input id="date" name="date" type="text" value="$date" placeholder="optional" pattern="\d{1,4}(-\d{2}(-\d{2})?)?" />
<label for="date"> Start date of material (YYYY-MM-DD, YYYY-MM or YYYY) - optional</label>

<input id="no_of_images" name="no_of_images" type="text" value="$no_of_images" placeholder="optional" pattern="\d{1,4}" />
<label for="no_of_images"> Number of images (normally double pages) - optional</label>

<input id="folder_id" name="folder_id" type="text" value="$folder_id" placeholder="optional" pattern="co/\d{6}" />
<label for="folder_id"> PM20 folder ID (e.g., co/012345) - optional</label>

<input id="search_name" name="search_name" type="text" value="$search_name" placeholder="optional" />
<label for="search_name"> Use alternative name for lookup - optional</label>

<button type="submit">Reconcile</button>

EOL;
}

function output_fieldset($matches) {

  $wd_base = 'https://wikidata.org/entity/';

  echo '<fieldset>';
  foreach ($matches as $key => $entry) {
    $qid = $entry->{'id'};
    $url = $wd_base . $qid;
    echo '<input type="radio" name="item" id="' . $qid . '" value="' . $qid . '" />';
    echo '<label for="' . $qid . '"><a href="' . $url . '" target="_blank">' . $entry->{'name'} . '</a> (' . $entry->{'description'} . '; score: ' . $entry->{'score'} . ')</label>';
  }
  echo <<<EOL2
    <input type="radio" name="item" value="new" id="check0">
    <label for="check0">Create new Wikidata item</label>
    </fieldset>

    <button type="submit">Create QS</button>
    EOL2;
}

function output_footer($debug) {

  echo '</form>';

  if ($debug) {
    echo <<<EOL3

      <h3>Debug</h3>
      <pre>
    EOL3;
    print_r( $_POST);
    echo '</pre>';
  }

  echo <<<EOL4

    </body>
    </html>
  EOL4;
}

function format_wd_date($date) {
  preg_match('/^(\d{4}(-\d{2}(-\d{2})?}?))$/', $date, $matches, PREG_UNMATCHED_AS_NULL);

  if ($matches[3]) {
    $date = "${date}T00:00:00Z/11";
  } elseif ($matches[2]) {
    $date = "${date}-00T00:00:00Z/10";
  } else {
    $date = "${date}-00-00T00:00:00Z/9";
  }
  return "+$date";
}
 
