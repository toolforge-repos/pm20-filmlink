---
title: Link PM20 film sections
lang: en

---

Link existing Wikidata items or create new items from [digitized microfilms](https://pm20.zbw.eu/film) of 20th Century Press Archives.

* [Companies/organizations](company_reconcile.php)

